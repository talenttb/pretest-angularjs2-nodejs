var http = require("http");
var handleRequest = function (request, response) {
    // console.log(`${new Date()} -> ${request.method} - path:${request.url}`)
    //if route match, return undefined,
    //else "N"
    if (!require("./configs/ngResourceRoutes")(request, response)) {
        return;
    }
    else if (!require("./configs/apiRoutes")(request, response)) {
        return;
    }
    else if (!require("./configs/htmlRoutes")(request, response)) {
        return;
    }
    response.writeHead(404);
    response.end();
};
var PORT = 10000;
//Create a server
var server = http.createServer(handleRequest);
//Lets start our server
server.listen(PORT, function () {
    //Callback triggered when server is successfully listening.
    console.log("Server listening on: http://localhost:" + PORT);
});
//# sourceMappingURL=server.js.map