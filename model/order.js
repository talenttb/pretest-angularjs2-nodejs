"use strict";
var OrderModel = (function () {
    // 訂單：訂單序號 / 訂單狀態（未付款/已付款) / 產品名稱 / 訂購數量 / 訂購會員名稱 / 訂購會員email
    function OrderModel(sqNo, paymentStatus, prdName, orderQty, orderMemName, orderMemEmail) {
        this.sqNo = sqNo;
        this.paymentStatus = paymentStatus;
        this.prdName = prdName;
        this.orderQty = orderQty;
        this.orderMemName = orderMemName;
        this.orderMemEmail = orderMemEmail;
    }
    return OrderModel;
}());
exports.OrderModel = OrderModel;
//# sourceMappingURL=order.js.map