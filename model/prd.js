"use strict";
var PrdModel = (function () {
    //產品欄位：產品名稱 / 產品類型(單選)(專用/泛用) / 產品屬性(複選)(紅/白/黃) / 產品單價 / 產品數量 / 銷售狀態(單選)(可賣/待售)
    function PrdModel(prdName, prdType, prdAttr, prdPrice, prdQty, salesStatus) {
        this.prdName = prdName;
        this.prdType = prdType;
        this.prdAttr = prdAttr;
        this.prdPrice = prdPrice;
        this.prdQty = prdQty;
        this.salesStatus = salesStatus;
    }
    return PrdModel;
}());
exports.PrdModel = PrdModel;
//# sourceMappingURL=prd.js.map