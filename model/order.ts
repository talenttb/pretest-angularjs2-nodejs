
export class OrderModel {
    sqNo:number;
    paymentStatus:boolean;
    prdName:string;
    orderQty:number;
    orderMemName:string;
    orderMemEmail:string;

    // 訂單：訂單序號 / 訂單狀態（未付款/已付款) / 產品名稱 / 訂購數量 / 訂購會員名稱 / 訂購會員email

    constructor (sqNo:number,paymentStatus:boolean,prdName:string,orderQty:number,orderMemName:string,orderMemEmail:string) {
        this.sqNo = sqNo;
        this.paymentStatus = paymentStatus;
        this.prdName = prdName;
        this.orderQty = orderQty;
        this.orderMemName = orderMemName;
        this.orderMemEmail = orderMemEmail;
    }
}