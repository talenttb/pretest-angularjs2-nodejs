
export class PrdModel {
    prdName:string;
    prdType:number;
    prdAttr:number;
    prdPrice:number;
    prdQty:number;
    salesStatus:number;

//產品欄位：產品名稱 / 產品類型(單選)(專用/泛用) / 產品屬性(複選)(紅/白/黃) / 產品單價 / 產品數量 / 銷售狀態(單選)(可賣/待售)

    constructor (prdName:string,prdType:number,prdAttr:number,prdPrice:number,prdQty:number,salesStatus:number) {
        this.prdName = prdName;
        this.prdType = prdType;
        this.prdAttr = prdAttr;
        this.prdPrice = prdPrice;
        this.prdQty = prdQty;
        this.salesStatus = salesStatus;
    }
}

