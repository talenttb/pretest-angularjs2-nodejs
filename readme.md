# Pre-test project

## 程式架構提要

* server.js為伺服器啟動點
* config為伺服器想關設定檔
* node_modules 前後端參考均同一個位置
* public為HTML靜態檔案
* public/app為前端Angular JS 2的資料夾

```
pretest
|   README.md
|   server.js
|---node_modeules
|
|---public
    |
    |---app
        |---
    |---css
        |---
    |---index.html
|---config
```

## 開啟

1. 下載node_modules
    ```
    npm install
    ```

1. 執行伺服器
    ```
    node server.js
    ```

1. 開啟瀏覽器，並輸入 http://localhost:10000


ps
tab下的其他tag未將檔案分層
