"use strict";
var mem_1 = require("../model/mem");
var order_1 = require("../model/order");
var prd_1 = require("../model/prd");
var Cache = (function () {
    /**
     *
     */
    function Cache() {
        this.memCache = [];
        this.prdCache = [];
        this.orderCache = [];
        this.memCache.push(new mem_1.MemModel("alex", "alex@test.com"));
        this.memCache.push(new mem_1.MemModel("mary", "mary@test.com"));
        this.memCache.push(new mem_1.MemModel("sam", "sam@test.com"));
        this.memCache.push(new mem_1.MemModel("jason", "jason@test.com"));
        this.memCache.push(new mem_1.MemModel("alen", "alen@test.com"));
        this.prdCache.push(new prd_1.PrdModel("Product 1", 1, 2, 100, 50000, 1));
        this.prdCache.push(new prd_1.PrdModel("Product 2", 2, 3, 80, 50000, 1));
        this.prdCache.push(new prd_1.PrdModel("Product 3", 2, 7, 1000, 500, 1));
        this.prdCache.push(new prd_1.PrdModel("Product 4", 1, 6, 100, 500, 2));
        this.orderCache.push(new order_1.OrderModel(1000, false, "Product 1", 10, "sam", "sam@test.com"));
        this.orderCache.push(new order_1.OrderModel(1001, false, "Product 2", 4, "jason", "jason@test.com"));
        this.orderCache.push(new order_1.OrderModel(1002, false, "Product 1", 2, "alen", "alen@test.com"));
        this.orderCache.push(new order_1.OrderModel(1003, true, "Product 2", 8, "mary", "mary@test.com"));
        this.orderCache.push(new order_1.OrderModel(1004, false, "Product 1", 20, "sam", "sam@test.com"));
        this.orderCache.push(new order_1.OrderModel(1005, false, "Product 3", 15, "alen", "alen@test.com"));
        this.orderCache.push(new order_1.OrderModel(1006, true, "Product 3", 6, "alex", "alex@test.com"));
        this.orderCache.push(new order_1.OrderModel(1007, true, "Product 3", 4, "mary", "mary@test.com"));
        this.orderCache.push(new order_1.OrderModel(1008, false, "Product 1", 9, "jason", "jason@test.com"));
        this.orderCache.push(new order_1.OrderModel(1009, true, "Product 2", 3, "alen", "alen@test.com"));
        this.orderCache.push(new order_1.OrderModel(1010, true, "Product 1", 13, "sam", "sam@test.com"));
        this.orderCache.push(new order_1.OrderModel(1011, false, "Product 1", 1, "alex", "alex@test.com"));
        this.orderCache.push(new order_1.OrderModel(1012, false, "Product 3", 2, "sam", "sam@test.com"));
        this.orderCache.push(new order_1.OrderModel(1013, true, "Product 1", 5, "jason", "jason@test.com"));
        this.orderCache.push(new order_1.OrderModel(1014, false, "Product 2", 9, "alen", "alen@test.com"));
        this.orderCache.push(new order_1.OrderModel(1015, true, "Product 2", 11, "mary", "mary@test.com"));
        this.orderCache.push(new order_1.OrderModel(1016, true, "Product 2", 10, "sam", "sam@test.com"));
        this.orderCache.push(new order_1.OrderModel(1017, false, "Product 1", 2, "jason", "jason@test.com"));
        this.orderCache.push(new order_1.OrderModel(1018, true, "Product 3", 3, "alen", "alen@test.com"));
    }
    Cache.prototype.PrdGet = function () {
        return this.prdCache.slice(0);
    };
    Cache.prototype.OrderGet = function () {
        return this.orderCache.slice(0);
    };
    Cache.prototype.MemAdd = function (val) {
        this.memCache.push(val);
    };
    Cache.prototype.MemGet = function () {
        return this.memCache.slice(0);
    };
    Cache.prototype.MemDel = function (email) {
        var removeIdx = -1;
        this.memCache.forEach(function (val, idx) {
            if (val.email === email) {
                removeIdx = idx;
                return;
            }
        });
        if (removeIdx === -1) {
            return;
        }
        this.memCache = this.memCache.slice(0, removeIdx).concat(this.memCache.slice(removeIdx + 1));
    };
    Cache.prototype.MemUpdate = function (email, newName, oldName) {
        // this.memCache.map(function (item) {
        //     if (item.email !== email) {
        //         return item;
        //     }
        //     // return {
        //     //     ...item,
        //     // name:newName
        //     // };
        //     return new MemModel(newName, email);
        // });
        var updateIdx = -1;
        this.memCache.forEach(function (val, idx) {
            if (val.email === email) {
                updateIdx = idx;
                return;
            }
        });
        if (updateIdx === -1) {
            return;
        }
        this.memCache[updateIdx] = new mem_1.MemModel(newName, email);
    };
    return Cache;
}());
exports.Cache = Cache;
//# sourceMappingURL=cache.js.map