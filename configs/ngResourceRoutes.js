var fs = require("fs");
var path = require("path");

module.exports = (req, resp) => {
	var url = req.url;
	var resouceStream;

	if (/^\/node_modules\/.+/.test(url)) {
		resouceStream = fs.createReadStream(path.join(".", req.url));
	} else if (url === "/systemjs.config.js") {
		resouceStream = fs.createReadStream(path.join(".", req.url));
	} else if (/^\/app\/.+/.test(url)) {
		resouceStream = fs.createReadStream(path.join("./public/", req.url));
	} else {
		return 'N';
	}

	resouceStream.on("open", () => {
		resp.writeHeader(200, {
			"Content-Type": "text/javascript"
		});
		resouceStream.pipe(resp);
	});

	resouceStream.on("error", (err) => {
		console.log("ERROR:" + err);
		resp.writeHead(400, {
			"content-type": "text/plain"
		});
		resp.end("no resouce.");
	});
}