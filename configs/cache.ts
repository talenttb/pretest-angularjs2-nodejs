import { MemModel } from "../model/mem";
import { OrderModel } from "../model/order";
import { PrdModel } from "../model/prd";


export class Cache {

    private memCache: any = [];
    private prdCache: any = [];
    private orderCache: any = [];

    /**
     *
     */
    constructor() {
        this.memCache.push(new MemModel("alex", "alex@test.com"));
        this.memCache.push(new MemModel("mary", "mary@test.com"));
        this.memCache.push(new MemModel("sam", "sam@test.com"));
        this.memCache.push(new MemModel("jason", "jason@test.com"));
        this.memCache.push(new MemModel("alen", "alen@test.com"));

        this.prdCache.push(new PrdModel("Product 1",1,2,100,50000,1));
        this.prdCache.push(new PrdModel("Product 2",2,3,80,50000,1));
        this.prdCache.push(new PrdModel("Product 3",2,7,1000,500,1));
        this.prdCache.push(new PrdModel("Product 4",1,6,100,500,2));

        this.orderCache.push(new OrderModel(1000,false,"Product 1",10,"sam","sam@test.com"));
        this.orderCache.push(new OrderModel(1001,false,"Product 2",4,"jason","jason@test.com"));
        this.orderCache.push(new OrderModel(1002,false,"Product 1",2,"alen","alen@test.com"));
        this.orderCache.push(new OrderModel(1003,true,"Product 2",8,"mary","mary@test.com"));
        this.orderCache.push(new OrderModel(1004,false,"Product 1",20,"sam","sam@test.com"));
        this.orderCache.push(new OrderModel(1005,false,"Product 3",15,"alen","alen@test.com"));
        this.orderCache.push(new OrderModel(1006,true,"Product 3",6,"alex","alex@test.com"));
        this.orderCache.push(new OrderModel(1007,true,"Product 3",4,"mary","mary@test.com"));
        this.orderCache.push(new OrderModel(1008,false,"Product 1",9,"jason","jason@test.com"));
        this.orderCache.push(new OrderModel(1009,true,"Product 2",3,"alen","alen@test.com"));
        this.orderCache.push(new OrderModel(1010,true,"Product 1",13,"sam","sam@test.com"));
        this.orderCache.push(new OrderModel(1011,false,"Product 1",1,"alex","alex@test.com"));
        this.orderCache.push(new OrderModel(1012,false,"Product 3",2,"sam","sam@test.com"));
        this.orderCache.push(new OrderModel(1013,true,"Product 1",5,"jason","jason@test.com"));
        this.orderCache.push(new OrderModel(1014,false,"Product 2",9,"alen","alen@test.com"));
        this.orderCache.push(new OrderModel(1015,true,"Product 2",11,"mary","mary@test.com"));
        this.orderCache.push(new OrderModel(1016,true,"Product 2",10,"sam","sam@test.com"));
        this.orderCache.push(new OrderModel(1017,false,"Product 1",2,"jason","jason@test.com"));
        this.orderCache.push(new OrderModel(1018,true,"Product 3",3,"alen","alen@test.com"));

    }

    public PrdGet(){
        return this.prdCache.slice(0);
    }

    public OrderGet(){
        return this.orderCache.slice(0);
    }

    public MemAdd(val: Object) {
        this.memCache.push(val);
    }

    public MemGet() {
        return this.memCache.slice(0);
    }

    public MemDel(email: string) {

        let removeIdx = -1;
        this.memCache.forEach((val, idx) => {

            if (val.email === email) {
                removeIdx = idx;
                return;
            }
        });

        if (removeIdx === -1) { return; }

        this.memCache = [...this.memCache.slice(0, removeIdx), ...this.memCache.slice(removeIdx + 1)];

    }

    public MemUpdate(email, newName, oldName: string) {

        // this.memCache.map(function (item) {
        //     if (item.email !== email) {
        //         return item;
        //     }
        //     // return {
        //     //     ...item,
        //     // name:newName
        //     // };
        //     return new MemModel(newName, email);
        // });

        let updateIdx = -1;
        this.memCache.forEach((val, idx) => {

            if (val.email === email) {
                updateIdx = idx;
                return;
            }
        });

        if (updateIdx === -1) { return; }

        this.memCache[updateIdx] = new MemModel(newName,email);
    }
}