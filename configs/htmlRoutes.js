var fs = require("fs");

module.exports = (req, resp) => {
	if (req.url === "/index.html" || req.url === "/") {
		resp.writeHeader(200, {
			"Content-Type": "text/html"
		});
		fs.createReadStream("./public/index.html").pipe(resp);
	} else if (req.url === "/htmlComponent/tabs.html") {
		resp.writeHeader(200, {
			"Content-Type": "text/css"
		});
		fs.createReadStream("./public" + req.url).pipe(resp);
	} else if (req.url === "/css/styles.css") {
		resp.writeHeader(200, {
			"Content-Type": "text/css"
		});
		fs.createReadStream("./public" + req.url).pipe(resp);
	} else {
		//no match
		return 'N';
	}
}