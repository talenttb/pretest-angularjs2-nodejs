var fs = require("fs");

import { Cache } from "./cache";
import { MemModel } from "../model/mem";

let c = new Cache();

module.exports = (req, resp) => {
	if (req.url === "/api/pretest") {
		resp.writeHead(200, {
			"content-type": "application/json"
		});
		resp.end(JSON.stringify({
			memberAPI: "/api/member",
			prdAPI: "/api/prd",
			orderAPI: "/api/order"
		}));
	} else if (req.url === "/api/member/list") {
		resp.writeHead(200, {
			"content-type": "application/json"
		});
		resp.end(JSON.stringify({ data: c.MemGet() }));
	} else if (req.url === "/api/member/edit" && req.method === 'POST') {
		let data = "";
		req.on('data', function (chunk) {
			data = chunk.toString();
		});

		req.on('end', function () {
			// empty 200 OK response for now

			let mem = JSON.parse(data);
			c.MemAdd(new MemModel(mem.n, mem.e));

			resp.writeHead(200, {
				"content-type": "application/json"
			});
			resp.end(JSON.stringify({ data: "OK" }));
		});
	} else if (req.url === "/api/member/edit" && req.method === 'PUT') {
		let data = "";
		req.on('data', function (chunk) {
			data = chunk.toString();
		});

		req.on('end', function () {
			// empty 200 OK response for now

			let mem = JSON.parse(data);
			c.MemUpdate(mem.email, mem.editName, mem.name);

			resp.writeHead(200, {
				"content-type": "application/json"
			});
			resp.end(JSON.stringify({ data: "OK" }));
		});


	} else if (/^\/api\/member\/edit\/.+/.test(req.url) && req.method === 'DELETE') {
		c.MemDel(req.url.slice(17));

		resp.writeHead(200, {
			"content-type": "application/json"
		});
		resp.end(JSON.stringify({ data: "OK" }));
	}else if (req.url === "/api/order/list") {
		resp.writeHead(200, {
			"content-type": "application/json"
		});
		resp.end(JSON.stringify({ data: c.OrderGet() }));
	} else if (req.url === "/api/order/edit" && req.method === 'POST') {
		let data = "";
		req.on('data', function (chunk) {
			data = chunk.toString();
		});

		req.on('end', function () {
			// empty 200 OK response for now

			let mem = JSON.parse(data);
			c.MemAdd(new MemModel(mem.n, mem.e));

			resp.writeHead(200, {
				"content-type": "application/json"
			});
			resp.end(JSON.stringify({ data: "OK" }));
		});
	} else if (req.url === "/api/order/edit" && req.method === 'PUT') {
		let data = "";
		req.on('data', function (chunk) {
			data = chunk.toString();
		});

		req.on('end', function () {
			// empty 200 OK response for now

			let mem = JSON.parse(data);
			c.MemUpdate(mem.email, mem.editName, mem.name);

			resp.writeHead(200, {
				"content-type": "application/json"
			});
			resp.end(JSON.stringify({ data: "OK" }));
		});


	} else if (/^\/api\/order\/edit\/.+/.test(req.url) && req.method === 'DELETE') {
		c.MemDel(req.url.slice(17));

		resp.writeHead(200, {
			"content-type": "application/json"
		});
		resp.end(JSON.stringify({ data: "OK" }));
	}else if (req.url === "/api/prd/list") {
		resp.writeHead(200, {
			"content-type": "application/json"
		});
		resp.end(JSON.stringify({ data: c.PrdGet() }));
	} else {
		//no match
		return 'N';
	}
}
