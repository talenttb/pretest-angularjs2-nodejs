"use strict";
var fs = require("fs");
var cache_1 = require("./cache");
var mem_1 = require("../model/mem");
var c = new cache_1.Cache();
module.exports = function (req, resp) {
    if (req.url === "/api/pretest") {
        resp.writeHead(200, {
            "content-type": "application/json"
        });
        resp.end(JSON.stringify({
            memberAPI: "/api/member",
            prdAPI: "/api/prd",
            orderAPI: "/api/order"
        }));
    }
    else if (req.url === "/api/member/list") {
        resp.writeHead(200, {
            "content-type": "application/json"
        });
        resp.end(JSON.stringify({ data: c.MemGet() }));
    }
    else if (req.url === "/api/member/edit" && req.method === 'POST') {
        var data_1 = "";
        req.on('data', function (chunk) {
            data_1 = chunk.toString();
        });
        req.on('end', function () {
            // empty 200 OK response for now
            var mem = JSON.parse(data_1);
            c.MemAdd(new mem_1.MemModel(mem.n, mem.e));
            resp.writeHead(200, {
                "content-type": "application/json"
            });
            resp.end(JSON.stringify({ data: "OK" }));
        });
    }
    else if (req.url === "/api/member/edit" && req.method === 'PUT') {
        var data_2 = "";
        req.on('data', function (chunk) {
            data_2 = chunk.toString();
        });
        req.on('end', function () {
            // empty 200 OK response for now
            var mem = JSON.parse(data_2);
            c.MemUpdate(mem.email, mem.editName, mem.name);
            resp.writeHead(200, {
                "content-type": "application/json"
            });
            resp.end(JSON.stringify({ data: "OK" }));
        });
    }
    else if (/^\/api\/member\/edit\/.+/.test(req.url) && req.method === 'DELETE') {
        c.MemDel(req.url.slice(17));
        resp.writeHead(200, {
            "content-type": "application/json"
        });
        resp.end(JSON.stringify({ data: "OK" }));
    }
    else if (req.url === "/api/order/list") {
        resp.writeHead(200, {
            "content-type": "application/json"
        });
        resp.end(JSON.stringify({ data: c.OrderGet() }));
    }
    else if (req.url === "/api/order/edit" && req.method === 'POST') {
        var data_3 = "";
        req.on('data', function (chunk) {
            data_3 = chunk.toString();
        });
        req.on('end', function () {
            // empty 200 OK response for now
            var mem = JSON.parse(data_3);
            c.MemAdd(new mem_1.MemModel(mem.n, mem.e));
            resp.writeHead(200, {
                "content-type": "application/json"
            });
            resp.end(JSON.stringify({ data: "OK" }));
        });
    }
    else if (req.url === "/api/order/edit" && req.method === 'PUT') {
        var data_4 = "";
        req.on('data', function (chunk) {
            data_4 = chunk.toString();
        });
        req.on('end', function () {
            // empty 200 OK response for now
            var mem = JSON.parse(data_4);
            c.MemUpdate(mem.email, mem.editName, mem.name);
            resp.writeHead(200, {
                "content-type": "application/json"
            });
            resp.end(JSON.stringify({ data: "OK" }));
        });
    }
    else if (/^\/api\/order\/edit\/.+/.test(req.url) && req.method === 'DELETE') {
        c.MemDel(req.url.slice(17));
        resp.writeHead(200, {
            "content-type": "application/json"
        });
        resp.end(JSON.stringify({ data: "OK" }));
    }
    else if (req.url === "/api/prd/list") {
        resp.writeHead(200, {
            "content-type": "application/json"
        });
        resp.end(JSON.stringify({ data: c.PrdGet() }));
    }
    else {
        //no match
        return 'N';
    }
};
//# sourceMappingURL=apiRoutes.js.map