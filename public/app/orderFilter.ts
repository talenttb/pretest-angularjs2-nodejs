import {Pipe} from '@angular/core';

@Pipe({
    name: 'OrderPipe'
})
export class OrderPipe {

    transform(list, args) {
        //0:mode
        //1:filter word
        //2:sort mode
        var filterWord = args[1];
        var sortQtyMode = args[2];
        var paymentStatus = args[3];

        var data = list.filter(item => {
            if (filterWord === undefined || filterWord === '') {
                return item;
            }

            switch (args[0]) {
                //產品編號
                case "1":
                    return (item.sqNo + '').indexOf(args[1]) > -1;
                //會員名稱
                case "2":
                    return item.orderMemName.indexOf(args[1]) > -1;
            }
        });



        data = data.filter(item=>{
            switch (paymentStatus) {
                //已付
                case "1":
                    return item.paymentStatus;
                //未付
                case "2":
                    return !item.paymentStatus;
                default:
                    return item;
            }
        });

        return data.sort((a, b) => {
            if (sortQtyMode === '1') {
                return b.orderQty - a.orderQty;
            } else if (sortQtyMode === '2') {
                return a.orderQty - b.orderQty;
            }
        });
    }

}