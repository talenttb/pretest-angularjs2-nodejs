"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var OrderPipe = (function () {
    function OrderPipe() {
    }
    OrderPipe.prototype.transform = function (list, args) {
        //0:mode
        //1:filter word
        //2:sort mode
        var filterWord = args[1];
        var sortQtyMode = args[2];
        var paymentStatus = args[3];
        var data = list.filter(function (item) {
            if (filterWord === undefined || filterWord === '') {
                return item;
            }
            switch (args[0]) {
                //產品編號
                case "1":
                    return (item.sqNo + '').indexOf(args[1]) > -1;
                //會員名稱
                case "2":
                    return item.orderMemName.indexOf(args[1]) > -1;
            }
        });
        data = data.filter(function (item) {
            switch (paymentStatus) {
                //已付
                case "1":
                    return item.paymentStatus;
                //未付
                case "2":
                    return !item.paymentStatus;
                default:
                    return item;
            }
        });
        return data.sort(function (a, b) {
            if (sortQtyMode === '1') {
                return b.orderQty - a.orderQty;
            }
            else if (sortQtyMode === '2') {
                return a.orderQty - b.orderQty;
            }
        });
    };
    OrderPipe = __decorate([
        core_1.Pipe({
            name: 'OrderPipe'
        }), 
        __metadata('design:paramtypes', [])
    ], OrderPipe);
    return OrderPipe;
}());
exports.OrderPipe = OrderPipe;
//# sourceMappingURL=orderFilter.js.map