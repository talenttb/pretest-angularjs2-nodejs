import { bootstrap }    from '@angular/platform-browser-dynamic';
// import { Tabs } from './tab.component';
// bootstrap(Tabs);

import {HTTP_PROVIDERS} from '@angular/http';
import {App} from './app';
bootstrap(App, [HTTP_PROVIDERS])
  .catch(console.error);