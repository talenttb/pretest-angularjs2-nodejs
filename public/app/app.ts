//our root app component
import {Component, ViewChild, Renderer, Input } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import {Tabs} from './tabs';
import {Tab} from './tab';
import {OrderPipe} from './orderFilter';

@Component({
    selector: 'my-app',
    templateUrl: '../htmlComponent/tabs.html',
    directives: [Tabs, Tab],
    pipes: [OrderPipe]
})

export class App {
    http: Http;
    private memberApiRoot = "";
    private orderApiRoot = "";
    private prdApiRoot = "";
    memberList = [];
    orderList = [];
    prdList = [];
    @ViewChild('newName') newName;
    @ViewChild('newEmail') newEmail;

    @Input() display = [];
    selectedMode = "1";
    //ENUM改寫
    //0:未排序
    //1:高到低
    //2:低到高
    sortQtyMode = "0";

    constructor(http: Http, renderer: Renderer) {
        this.http = http;
    }
    ngOnInit() {
        this.getRootAPI();
    }

    getRootAPI() {
        this.http.get('/api/pretest')
            .map((res: Response) => res.json())
            .subscribe(
            data => {
                this.memberApiRoot = data.memberAPI;
                this.getMemList();

                this.orderApiRoot = data.orderAPI;
                this.getOrderList();

                this.prdApiRoot = data.prdAPI;
                this.getPrdList();


            },
            err => console.error(err),
            () => console.log('getRootAPI done')
            );
    }

    getPrdList(){
        this.http.get(`${this.prdApiRoot}/list`)
            .map((res: Response) => res.json())
            .subscribe(
            data => {
                this.prdList = [];
                data.data.map((val) => {
                    this.prdList.push(
                        {
                            prdName: val.prdName,
                            prdType: val.prdType,
                            prdAttr: val.prdAttr,
                            prdAttrPaser:((attr)=>{
                                var result = [],tmp = ~~attr ;
                                if(tmp&1){result.push('黃');}
                                if(tmp&2){result.push('白');}
                                if(tmp&4){result.push('紅');}
                                return result.join('/');
                            })(val.prdAttr),
                            prdPrice: val.prdPrice,
                            prdQty: val.prdQty,
                            salesStatus: val.salesStatus
                        }
                    );
                });
            },
            err => console.error(err)
            );
    }

    getOrderList(){
        this.http.get(`${this.orderApiRoot}/list`)
            .map((res: Response) => res.json())
            .subscribe(
            data => {
                // this.display = [];
                this.orderList = [];
                data.data.map((val) => {
                    this.orderList.push(
                        {
                            sqNo: val.sqNo,
                            paymentStatus: val.paymentStatus,
                            prdName: val.prdName,
                            orderQty: val.orderQty,
                            orderMemName: val.orderMemName,
                            orderMemEmail: val.orderMemEmail
                        }
                    );
                    // this.display.push(true);
                });
            },
            err => console.error(err)
            );
    }

    sortQty(){
        if(this.sortQtyMode==='0'){
            this.sortQtyMode='1';
        }else if(this.sortQtyMode==='1'){
            this.sortQtyMode='2';
        }else if(this.sortQtyMode==='2'){
            this.sortQtyMode='1';
        }
    }

    getMemList() {
        this.http.get(`${this.memberApiRoot}/list`)
            .map((res: Response) => res.json())
            .subscribe(
            data => {
                this.display = [];
                this.memberList = [];
                data.data.map((val) => {
                    this.memberList.push(
                        {
                            name: val.name,
                            email: val.email,
                            editName: val.name,
                        }
                    );
                    this.display.push(true);
                });
            },
            err => console.error(err),
            () => console.log('getMemList done')
            );
    }

    deleteMember(mem) {
        // console.log(`delete ${mem.name},${mem.email}`);
        if (confirm('Are you sure you want to delete user ' + mem.name)) {
            this.http.delete(`${this.memberApiRoot}/edit/${mem.email}`)
                .subscribe(
                (response) => {
                    console.log(response);
                    this.getMemList();
                }
                );
        }
    }

    editMember(i: number) {
        this.display[i] = false;
    }

    saveMember(i: number) {
        this.display[i] = true;
        this.memberList[i].name = this.memberList[i].editName;
        this.http.put(`${this.memberApiRoot}/edit`, JSON.stringify(this.memberList[i]))
            .subscribe(
            (response) => {
                this.getMemList();
            }
            );
    }
    cancelMember(i: number) {
        this.display[i] = true;
        this.memberList[i].editName = this.memberList[i].name;
    }

    newMember() {
        // console.log(this.newName.nativeElement.value);

        this.http.post(`${this.memberApiRoot}/edit`, JSON.stringify({ n: this.newName.nativeElement.value, e: this.newEmail.nativeElement.value }))
            .subscribe(
            (response) => {
                this.getMemList();
            }
            );

    }
}