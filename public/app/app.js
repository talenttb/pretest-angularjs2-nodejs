"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//our root app component
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/Rx');
var tabs_1 = require('./tabs');
var tab_1 = require('./tab');
var orderFilter_1 = require('./orderFilter');
var App = (function () {
    function App(http, renderer) {
        this.memberApiRoot = "";
        this.orderApiRoot = "";
        this.prdApiRoot = "";
        this.memberList = [];
        this.orderList = [];
        this.prdList = [];
        this.display = [];
        this.selectedMode = "1";
        //ENUM改寫
        //0:未排序
        //1:高到低
        //2:低到高
        this.sortQtyMode = "0";
        this.http = http;
    }
    App.prototype.ngOnInit = function () {
        this.getRootAPI();
    };
    App.prototype.getRootAPI = function () {
        var _this = this;
        this.http.get('/api/pretest')
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.memberApiRoot = data.memberAPI;
            _this.getMemList();
            _this.orderApiRoot = data.orderAPI;
            _this.getOrderList();
            _this.prdApiRoot = data.prdAPI;
            _this.getPrdList();
        }, function (err) { return console.error(err); }, function () { return console.log('getRootAPI done'); });
    };
    App.prototype.getPrdList = function () {
        var _this = this;
        this.http.get(this.prdApiRoot + "/list")
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.prdList = [];
            data.data.map(function (val) {
                _this.prdList.push({
                    prdName: val.prdName,
                    prdType: val.prdType,
                    prdAttr: val.prdAttr,
                    prdAttrPaser: (function (attr) {
                        var result = [], tmp = ~~attr;
                        if (tmp & 1) {
                            result.push('黃');
                        }
                        if (tmp & 2) {
                            result.push('白');
                        }
                        if (tmp & 4) {
                            result.push('紅');
                        }
                        return result.join('/');
                    })(val.prdAttr),
                    prdPrice: val.prdPrice,
                    prdQty: val.prdQty,
                    salesStatus: val.salesStatus
                });
            });
        }, function (err) { return console.error(err); });
    };
    App.prototype.getOrderList = function () {
        var _this = this;
        this.http.get(this.orderApiRoot + "/list")
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            // this.display = [];
            _this.orderList = [];
            data.data.map(function (val) {
                _this.orderList.push({
                    sqNo: val.sqNo,
                    paymentStatus: val.paymentStatus,
                    prdName: val.prdName,
                    orderQty: val.orderQty,
                    orderMemName: val.orderMemName,
                    orderMemEmail: val.orderMemEmail
                });
                // this.display.push(true);
            });
        }, function (err) { return console.error(err); });
    };
    App.prototype.sortQty = function () {
        if (this.sortQtyMode === '0') {
            this.sortQtyMode = '1';
        }
        else if (this.sortQtyMode === '1') {
            this.sortQtyMode = '2';
        }
        else if (this.sortQtyMode === '2') {
            this.sortQtyMode = '1';
        }
    };
    App.prototype.getMemList = function () {
        var _this = this;
        this.http.get(this.memberApiRoot + "/list")
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.display = [];
            _this.memberList = [];
            data.data.map(function (val) {
                _this.memberList.push({
                    name: val.name,
                    email: val.email,
                    editName: val.name,
                });
                _this.display.push(true);
            });
        }, function (err) { return console.error(err); }, function () { return console.log('getMemList done'); });
    };
    App.prototype.deleteMember = function (mem) {
        var _this = this;
        // console.log(`delete ${mem.name},${mem.email}`);
        if (confirm('Are you sure you want to delete user ' + mem.name)) {
            this.http.delete(this.memberApiRoot + "/edit/" + mem.email)
                .subscribe(function (response) {
                console.log(response);
                _this.getMemList();
            });
        }
    };
    App.prototype.editMember = function (i) {
        this.display[i] = false;
    };
    App.prototype.saveMember = function (i) {
        var _this = this;
        this.display[i] = true;
        this.memberList[i].name = this.memberList[i].editName;
        this.http.put(this.memberApiRoot + "/edit", JSON.stringify(this.memberList[i]))
            .subscribe(function (response) {
            _this.getMemList();
        });
    };
    App.prototype.cancelMember = function (i) {
        this.display[i] = true;
        this.memberList[i].editName = this.memberList[i].name;
    };
    App.prototype.newMember = function () {
        // console.log(this.newName.nativeElement.value);
        var _this = this;
        this.http.post(this.memberApiRoot + "/edit", JSON.stringify({ n: this.newName.nativeElement.value, e: this.newEmail.nativeElement.value }))
            .subscribe(function (response) {
            _this.getMemList();
        });
    };
    __decorate([
        core_1.ViewChild('newName'), 
        __metadata('design:type', Object)
    ], App.prototype, "newName", void 0);
    __decorate([
        core_1.ViewChild('newEmail'), 
        __metadata('design:type', Object)
    ], App.prototype, "newEmail", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], App.prototype, "display", void 0);
    App = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: '../htmlComponent/tabs.html',
            directives: [tabs_1.Tabs, tab_1.Tab],
            pipes: [orderFilter_1.OrderPipe]
        }), 
        __metadata('design:paramtypes', [http_1.Http, core_1.Renderer])
    ], App);
    return App;
}());
exports.App = App;
//# sourceMappingURL=app.js.map